﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace versionamento.Models
{
    public class Repositorios
    {
        public int id { get; set; }
        public string name { get; set; }
        public string version { get; set; }
        public string pulltitle { get; set; }
        public string link { get; set; }
    }
}