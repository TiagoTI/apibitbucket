﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SharpBucket.V2;
using versionamento.Models;

namespace versionamento.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                CatalogoDeProdutosEntities1 dbx = new CatalogoDeProdutosEntities1();

                int i = 0;
                var repositorios = new List<Repositorios>();
                var sharpBucket = new SharpBucketV2();
                sharpBucket.OAuth2ClientCredentials("6ewwFSyWdwCbCUmGBj", "u8WGYeHTWkjpbQTWPAessbCbMfuvdVjU");

                    List<Projeto> repositories = dbx.Projeto.ToList();

                    foreach (var repository in repositories) {
                        Repositorios repositorio = new Repositorios();

                        var tag = sharpBucket.RepositoriesEndPoint().TagResource("arcoeducaosistemas", repository.Codigo).ListTags().OrderByDescending(x => x.date).FirstOrDefault();
                        var pullRequest = sharpBucket.RepositoriesEndPoint().PullRequestsResource("arcoeducaosistemas", repository.Codigo).GetPullRequestLog();
                        var link = sharpBucket.RepositoriesEndPoint().RepositoryResource("arcoeducaosistemas", repository.Codigo).GetRepository();
                            repositorio.version = tag == null? "v0.0": tag.name;
                            repositorio.name = repository.Cabecalho;
                            repositorio.pulltitle = pullRequest.Count == 0 ? "Null" : CommonMark.CommonMarkConverter.Convert(pullRequest[0].pull_request.title);
                            repositorio.id = i;
                            repositorio.link = link.links.self.href;
                            i++;

                            repositorios.Add(repositorio);
                    }

                ViewBag.repo = repositorios;

                return View();
            }
            catch (Exception ex)
            {
                var teste = ex.Message;
                return View();

            }
        }

    }
}